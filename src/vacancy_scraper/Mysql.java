package vacancy_scraper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Mysql {
	private static final String url = "jdbc:mysql://localhost:3306/vacancies";
    private static final String user = "root";
    private static final String password = "root";
    
    private static Connection con;

	public boolean select() {
		String query_string = "select title, description, company from vacancies";

		Statement stmt = null;
		try {
			stmt = con.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    ResultSet rs = null;
		try {
			rs = stmt.executeQuery(query_string);

			if (rs.getFetchSize() == 0) { return false;}

			while (rs.next()) {
				String title = rs.getString(1);
				String description = rs.getString(2);
				String company = rs.getString(3);
				System.out.printf("title: %s, descr: %s, company: %s %n", title, description, company);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		try {
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public void update(String title, String description, String company) {
		String string_format = "insert into vacancies.vacancies (title, description, company) values ('%s', '%s', '%s');";
		String query_string = String.format(string_format, title, description, company);

		Statement stmt = null;
		try {
			stmt = con.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			stmt.executeUpdate(query_string);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean open_connect() {
		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void close_connect() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
