package vacancy_scraper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class Scraper {
	
	public static void main(String[] args) throws IOException {
		String search_text = "";
		if (args.length != 0 && args[0].contentEquals("search")) {
			search_text = args[1];
		}
//		String url = "https://nn.hh.ru/search/vacancy/?text=" + search_text + "&area=66";
		String url = "https://nn.hh.ru/search/vacancy?no_magic=true&items_on_page=100&enable_snippets=true&clusters=true&area=66&specialization=1.221&from=cluster_specialization&text=" + search_text;
		ConsoleOutput coutput = new ConsoleOutput();
	
		Mysql db = new Mysql();
		
		//TODO: wrap to try
		boolean conn = db.open_connect();
		boolean select_result = false;
		if (!conn) {
			System.out.println("Connection to database is failure");
//			System.exit(-1);
//			select_result = false;
		}
//		boolean select_result = db.select();
		if (conn) {
			select_result = db.select();
			db.close_connect();
		}
		
		if ( !select_result ) {
//			//HTMLUNIT http://htmlunit.sourceforge.net/

			List<Vacancy> vacancies = new ArrayList<Vacancy>();
			try {
				vacancies = getVacanciesFromFile();
			} catch (FileNotFoundException e) {
				ScrapyVac scraper = new ScrapyVac();
				vacancies = scraper.getVacancies(url);
			}


			//TODO: add check for list's size
			for (Vacancy vac : vacancies.subList(0, 49)) {
				coutput.Output(vac, search_text);
			}
		}
       
	}

	public static List<Vacancy> getVacanciesFromFile() throws IOException {
		List<Vacancy> vacancies = new ArrayList<Vacancy>();
		ObjectInputStream ois = null; 

		try {
			ois = new ObjectInputStream(new FileInputStream("vacs.ser"));
		} catch (FileNotFoundException e) {
//			ois.close(); ???
			throw e;
		}

		try {
			vacancies = (List<Vacancy>) ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			;
		}
		return vacancies;
	}

}
