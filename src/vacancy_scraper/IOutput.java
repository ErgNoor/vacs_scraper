package vacancy_scraper;

import java.util.List;

public interface IOutput {
	void Output(Vacancy vac, String search_text);
}
