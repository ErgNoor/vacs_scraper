package vacancy_scraper;

import java.io.Serializable;

public class Vacancy implements Serializable{
	public Vacancy(String title, String company, String description) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.company = company;
		this.description = description;
	}
	public String title = "";
	public String company = "";
	public String description = "";
}
