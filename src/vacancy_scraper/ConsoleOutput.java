package vacancy_scraper;

import java.util.List;

public class ConsoleOutput implements IOutput {
	public void Output(Vacancy vac, String search_text){
		if (search_text != "" && (vac.description.toLowerCase().contains(search_text)
				|| vac.title.toLowerCase().contains(search_text)
				|| vac.company.toLowerCase().contains(search_text))) {

			System.out.printf("title: %s", vac.title); System.out.print("\t");
    		System.out.printf("description: %s", vac.description); System.out.print("\t");
    		System.out.printf("company: %s", vac.company); System.out.print("\t\n");
    		System.out.println("%%%");
		}
	}
}
