package vacancy_scraper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.logging.LogFactory;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class ScrapyVac {
	
	public List<Vacancy> getVacancies(String url) {
		String vacs_xpath = "//div[contains(@class, 'search-result-item') and contains(@class, 'search-result-item_standard')]";
		String vac_title_xpath = "./div[@class='search-result-description']//div[@class='search-result-item__head']/a/text()";
		String vac_company_xpath = "./div[@class='search-result-description']//div[@class='search-result-item__company']/a[@data-qa='vacancy-serp__vacancy-employer']/text()";
		String vac_description_xpath = "./div[@class='search-result-description']//div[@class='search-result-item__snippet']/text()";
		
		List<Vacancy> vacancies = new ArrayList<Vacancy>();
		HtmlPage page = getPage(url);
		
		List<DomNode> vacs = (List<DomNode>) page.getByXPath(vacs_xpath);
		
		Mysql db = new Mysql();
		db.open_connect();
		
		for (DomNode vac: vacs) {
			String title = (String) vac.getByXPath(vac_title_xpath).get(0).toString();
			String company = (String) vac.getByXPath(vac_company_xpath).get(0).toString();
			String description = makeString((List<DomNode>) vac.getByXPath(vac_description_xpath));
			vacancies.add(new Vacancy(title, company, description));
//			db.update(title, description, company); // there are some bugs related to db architecture here :(
		}
		
		serializeVacancy(vacancies);
		
		db.close_connect();
				
		return vacancies;
	}

	private void serializeVacancy(List<Vacancy> vacancies) {
		try {
			FileOutputStream fout = new FileOutputStream("vacs.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(vacancies);
			oos.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	private String makeString(List<DomNode> descrs) {
		// TODO Auto-generated method stub
		String description = "";
		for (DomNode descr : descrs) {
			description += descr.toString();
		}
		return description;
	}

	private HtmlPage getPage(String url) {
		final WebClient webClient = new WebClient(BrowserVersion.CHROME);
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getOptions().setJavaScriptEnabled(true);
//        webClient.getOptions().setTimeout(2000);
        webClient.getOptions().setUseInsecureSSL(true);
        // overcome problems in JavaScript
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setPrintContentOnFailingStatusCode(false);
        webClient.setCssErrorHandler(new SilentCssErrorHandler());
        
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger
           .getLogger("com.gargoylesoftware.htmlunit")
           .setLevel(Level.OFF);
        java.util.logging.Logger
           .getLogger("org.apache.commons.httpclient")
           .setLevel(Level.OFF);
        java.util.logging.Logger
          .getLogger("com.gargoylesoftware.htmlunit.javascript.StrictErrorReporter")
          .setLevel(Level.OFF);
        java.util.logging.Logger
          .getLogger("com.gargoylesoftware.htmlunit.javascript.host.ActiveXObject")
          .setLevel(Level.OFF);
        java.util.logging.Logger
          .getLogger("com.gargoylesoftware.htmlunit.javascript.host.html.HTMLDocument")
          .setLevel(Level.OFF);
        java.util.logging.Logger
          .getLogger("com.gargoylesoftware.htmlunit.html.HtmlScript")
          .setLevel(Level.OFF);
        java.util.logging.Logger
          .getLogger("com.gargoylesoftware.htmlunit.javascript.host.WindowProxy")
          .setLevel(Level.OFF);
        java.util.logging.Logger
          .getLogger("org.apache")
          .setLevel(Level.OFF);
		
        HtmlPage page = null;
		try {
			page = webClient.getPage(url);
		} catch (FailingHttpStatusCodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return page;
	}

}
